# Postgrad Web API sample project

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Sample project for the final .NET assignment. It is a Web API with EF Core integrated. There are some extra features that arent needed for the assignment, like custom exceptions and returning ProblemDetails. What is required for the assignment is: Controllers, Services, Mappers, and DTOs.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

Open in VS2022, change connection string from application.properties. Then in package manager console and run:

```bash
update-database
```

## Maintainers

[@NicholasLennox](https://gitlab.com/NicholasLennox)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Noroff Accelerate
