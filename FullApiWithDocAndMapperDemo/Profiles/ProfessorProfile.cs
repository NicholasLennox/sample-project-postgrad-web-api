﻿using AutoMapper;
using FullApiWithDocAndMapperDemo.Models;
using FullApiWithDocAndMapperDemo.Models.Dtos.Professors;

namespace FullApiWithDocAndMapperDemo.Profiles
{
    /// <summary>
    /// Mappings for Professor entity to various DTOs
    /// </summary>
    public class ProfessorProfile : Profile
    {
        public ProfessorProfile()
        {          
            CreateMap<ProfessorPostDto, Professor>();

            CreateMap<Professor,ProfessorDto>()
                .ForMember(dto => dto.Students, opt => opt
                .MapFrom(p => p.Students.Select(s => s.Id).ToList()))
                .ForMember(dto => dto.Subjects, opt => opt
                .MapFrom(p => p.Subjects.Select(s => s.Id).ToList()));

            CreateMap<ProfessorPutDto, Professor>();
        }
    }
}
