﻿using AutoMapper;
using FullApiWithDocAndMapperDemo.Models;
using FullApiWithDocAndMapperDemo.Models.Dtos.Students;

namespace FullApiWithDocAndMapperDemo.Profiles
{
    public class StudentProfile : Profile
    {
        public StudentProfile()
        {
            CreateMap<Student, StudentSummaryDto>();
        }
    }
}
