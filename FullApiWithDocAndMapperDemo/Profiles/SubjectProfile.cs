﻿using AutoMapper;
using FullApiWithDocAndMapperDemo.Models;
using FullApiWithDocAndMapperDemo.Models.Dtos.Subjects;

namespace FullApiWithDocAndMapperDemo.Profiles
{
    public class SubjectProfile : Profile
    {
        public SubjectProfile()
        {
            CreateMap<Subject, SubjectSummaryDto>();
        }
    }
}
