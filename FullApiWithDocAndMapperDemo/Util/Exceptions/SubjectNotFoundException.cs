﻿namespace FullApiWithDocAndMapperDemo.Util.Exceptions
{
    public class SubjectNotFoundException : EntityNotFoundException
    {
        public SubjectNotFoundException() : base("No Subject found with provided Id.")
        {
        }
    }
}
