﻿namespace FullApiWithDocAndMapperDemo.Util.Exceptions
{
    public class StudentNotFoundException : EntityNotFoundException
    {
        public StudentNotFoundException() : base("No Student found with given Id.")
        {
        }
    }
}
