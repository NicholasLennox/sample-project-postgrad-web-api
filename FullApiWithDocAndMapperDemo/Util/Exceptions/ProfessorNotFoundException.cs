﻿namespace FullApiWithDocAndMapperDemo.Util.Exceptions
{
    public class ProfessorNotFoundException : EntityNotFoundException
    {
        public ProfessorNotFoundException() : base("Professor not found with that Id.")
        {
        }
    }
}
