﻿using FullApiWithDocAndMapperDemo.Models;
using FullApiWithDocAndMapperDemo.Util.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace FullApiWithDocAndMapperDemo.Services.Professors
{
    /// <summary>
    /// This class serves to house the business logic surrounding a professor.
    /// This class interacts with a repository pattern in the form of Entity Framework.
    /// It is also viable to separate data access into repositories as well, 
    /// however, it feels a but uneeded when using EF (it is already a repository pattern).
    /// </summary>
    public class ProfessorServiceImpl : IProfessorService
    {
        private readonly PostgradEfContext _postgradEfContext;
        // Log and throw is very common, but many developers dont like it.
        // You will see it in the methods where there is a possibility that a professor doesnt exist.
        // It could be refactored into a method, but having it placed clearly in the method helps readability.
        private readonly ILogger<ProfessorServiceImpl> _logger; 

        public ProfessorServiceImpl(PostgradEfContext postgradEfContext, ILogger<ProfessorServiceImpl> logger)
        {
            _postgradEfContext = postgradEfContext;
            _logger = logger;
        }

        public async Task AddAsync(Professor entity)
        {
            await _postgradEfContext.AddAsync(entity);
            await _postgradEfContext.SaveChangesAsync();
        }

        public async Task<ICollection<Professor>> GetAllAsync()
        {
            return await _postgradEfContext.Professors
                .Include(p => p.Students)
                .Include(p => p.Subjects)
                .ToListAsync();
        }

        public async Task<Professor> GetByIdAsync(int id)
        {
            // Log and throw error handling
            if (!await ProfessorExistsAsync(id))
            {
                _logger.LogError("Professor not found with Id: " + id);
                throw new ProfessorNotFoundException();
            }   
            // Want to include all related data for professor
            return await _postgradEfContext.Professors
                .Where(p => p.Id == id)
                .Include(p => p.Students)
                .Include(p => p.Subjects)
                .FirstAsync();
        }

        public async Task<ICollection<Student>> GetStudentsAsync(int professorId)
        {
            // Log and throw error handling
            if (!await ProfessorExistsAsync(professorId))
            {
                _logger.LogError("Professor not found with Id: " + professorId);
                throw new ProfessorNotFoundException();
            }
            // Dont need to include related data because of the DTO we are mapping to.
            // This can change depending on the business requirements.
            return await _postgradEfContext.Students
                .Where(s => s.ProfessorId == professorId)
                .ToListAsync();
        }

        public async Task<ICollection<Subject>> GetSubjectsAsync(int professorId)
        {
            // Log and throw error handling
            if (!await ProfessorExistsAsync(professorId))
            {
                _logger.LogError("Professor not found with Id: " + professorId);
                throw new ProfessorNotFoundException();
            }
            // Dont need to include related data because of the DTO we are mapping to.
            // This can change depending on the business requirements.
            return await _postgradEfContext.Subjects
                .Where(s => s.ProfessorId == professorId)
                .ToListAsync();
        }

        public async Task UpdateAsync(Professor entity)
        {
            // Log and throw pattern
            if (!await ProfessorExistsAsync(entity.Id))
            {
                _logger.LogError("Professor not found with Id: " + entity.Id);
                throw new ProfessorNotFoundException();
            }
            _postgradEfContext.Entry(entity).State = EntityState.Modified;
            await _postgradEfContext.SaveChangesAsync();
        }

        public async Task UpdateStudentsAsync(int[] studentIds, int professorId)
        {
            // Log and throw pattern
            if (!await ProfessorExistsAsync(professorId))
            {
                _logger.LogError("Professor not found with Id: " + professorId);
                throw new ProfessorNotFoundException();
            }    
            // First convert the int[] to List<Student>

            // Could utilize the StudentNotFoundException here
            // if there is an Id in the array that doesnt have a student.
            // That would require a different interaction with EF to check for exception.
            // It is not done in this implementation.
            List<Student> students = studentIds
                .ToList()
                .Select(sid => _postgradEfContext.Students
                .Where(s => s.Id == sid).First())
                .ToList();
            // Get professor for Id
            Professor professor = await _postgradEfContext.Professors
                .Where(p => p.Id == professorId)
                .FirstAsync();
            // Set the professors students
            professor.Students = students;
            _postgradEfContext.Entry(professor).State = EntityState.Modified;
            // Save all the changes
            await _postgradEfContext.SaveChangesAsync();
        }

        public async Task UpdateSubjectsAsync(int[] subjectIds, int professorId)
        {
            // Log and throw pattern
            if (!await ProfessorExistsAsync(professorId))
            {
                _logger.LogError("Professor not found with Id: " + professorId);
                throw new ProfessorNotFoundException();
            }
            // First convert the int[] to List<Subject>

            // Could utilize the SubjectNotFoundException here
            // if there is an Id in the array that doesnt have a subject.
            // That would require a different interaction with EF to check for exception.
            // It is not done in this implementation.
            List<Subject> subjects = subjectIds
                .ToList()
                .Select(sid => _postgradEfContext.Subjects
                .Where(s => s.Id == sid).First())
                .ToList();
            // Get professor for Id
            Professor professor = await _postgradEfContext.Professors
                .Where(p => p.Id == professorId)
                .FirstAsync();
            // Set the professors subjects
            professor.Subjects = subjects;
            _postgradEfContext.Entry(professor).State = EntityState.Modified;
            // Save all the changes
            await _postgradEfContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            var professor = await _postgradEfContext.Professors.FindAsync(id);
            // Log and throw pattern
            if (professor == null)
            {
                _logger.LogError("Professor not found with Id: " + id);
                throw new ProfessorNotFoundException();
            }
            // We set our entities to have nullable relationships
            // so it removes the FKs when delete it.
            _postgradEfContext.Professors.Remove(professor);
            await _postgradEfContext.SaveChangesAsync();
        }

        /// <summary>
        /// Simple utility method to help check if a professor exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If professor exists</returns>
        private async Task<bool> ProfessorExistsAsync(int id)
        {
            return await _postgradEfContext.Professors.AnyAsync(e => e.Id == id);
        }
    }
}
