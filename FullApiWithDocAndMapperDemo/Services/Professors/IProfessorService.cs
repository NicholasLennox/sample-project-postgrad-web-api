﻿using FullApiWithDocAndMapperDemo.Models;
using FullApiWithDocAndMapperDemo.Util.Exceptions;

namespace FullApiWithDocAndMapperDemo.Services.Professors
{
    public interface IProfessorService : ICrudService<Professor, int>
    {
        /// <summary>
        /// Gets all the students a professor supervises.
        /// </summary>
        /// <param name="professorId"></param>
        /// <returns>A collection of students</returns>
        /// <exception cref="ProfessorNotFoundException">Thrown when a professor does not exist with the specified Id in the database.</exception>
        Task<ICollection<Student>> GetStudentsAsync(int professorId);
        /// <summary>
        /// Gets all the subjects a professor teaches.
        /// </summary>
        /// <param name="professorId"></param>
        /// <returns>A collection of subjects</returns>
        /// <exception cref="ProfessorNotFoundException">Thrown when a professor does not exist with the specified Id in the database.</exception>
        Task<ICollection<Subject>> GetSubjectsAsync(int professorId);
        /// <summary>
        /// Updates the subjects a professor teaches. This is a complete replacement, so a full list is required.
        /// </summary>
        /// <param name="studentIds"></param>
        /// <param name="professorId"></param>
        /// <exception cref="ProfessorNotFoundException">Thrown when a professor does not exist with the specified Id in the database.</exception>
        Task UpdateStudentsAsync(int[] studentIds, int professorId);
        /// <summary>
        /// Updates the students a professor supervises. This is a complete replacement, so a full list is required.
        /// </summary>
        /// <param name="subjectIds"></param>
        /// <param name="professorId"></param>
        /// <exception cref="ProfessorNotFoundException">Thrown when a professor does not exist with the specified Id in the database.</exception>
        Task UpdateSubjectsAsync(int[] subjectIds, int professorId);
    }
}
