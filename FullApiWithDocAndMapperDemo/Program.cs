using FullApiWithDocAndMapperDemo.Models;
using FullApiWithDocAndMapperDemo.Services.Professors;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Configuring generated XML docs for Swagger

var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Postgrad API",
        Description = "Simple API to manage postgraduate studies",
        Contact = new OpenApiContact
        {
            Name = "Nicholas Lennox",
            Url = new Uri("https://gitlab.com/NicholasLennox")
        },
        License = new OpenApiLicense
        {
            Name = "MIT 2022",
            Url = new Uri("https://opensource.org/licenses/MIT")
        }
    });
    options.IncludeXmlComments(xmlPath);
});
// Adding services to the container
builder.Services.AddDbContext<PostgradEfContext>(
    opt => opt.UseSqlServer(
        builder.Configuration.GetConnectionString("PostgradDb")
        )
    );
// Automapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
// Adding logging through ILogger
builder.Host.ConfigureLogging(logging =>
{
    logging.ClearProviders();
    logging.AddConsole();
});
// Custom Services
builder.Services.AddTransient<IProfessorService, ProfessorServiceImpl>(); // Transient is the default behaviour and means a new instance is made when injected.

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

// Scaffold-DbContext "Data Source = ACC-NLENNOX\SQLEXPRESS; Initial Catalog = PostgradEf; Integrated Security = True;" Microsoft.EntityFrameworkCore.SqlServer -o Models

