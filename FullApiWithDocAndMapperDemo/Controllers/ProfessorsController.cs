﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FullApiWithDocAndMapperDemo.Models;
using FullApiWithDocAndMapperDemo.Models.Dtos.Professors;
using AutoMapper;
using FullApiWithDocAndMapperDemo.Services.Professors;
using FullApiWithDocAndMapperDemo.Util.Exceptions;
using FullApiWithDocAndMapperDemo.Models.Dtos.Students;
using FullApiWithDocAndMapperDemo.Models.Dtos.Subjects;
using System.Net;

namespace FullApiWithDocAndMapperDemo.Controllers
{
    /// <summary>
    /// Controller for Professor domain. 
    /// Some methods are not async, this is because the service returns nothing 
    /// and acts as a command. Those dont need to be wrapped in a task 
    /// because it doesnt need to wait for a returned value to continue.
    /// The compiler will suggest to make methods without await synchronous.
    /// 
    /// Have to include return types with ActionResult 
    /// (Not just using IActionResult for all endpoints) so swagger can 
    /// formulate response types.
    /// </summary>
    [Route("api/v1/professors")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ProfessorsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProfessorService _professorService;

        public ProfessorsController(IMapper mapper, IProfessorService professorService)
        {
            _mapper = mapper;
            _professorService = professorService;
        }

        /// <summary>
        /// Gets all the professors in the database with their related data represented as Ids.
        /// </summary>
        /// <returns>List of ProfessorDto</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProfessorDto>>> GetProfessors()
        {
            return Ok(
                _mapper.Map<List<ProfessorDto>>(
                    await _professorService.GetAllAsync())
                );
        }

        /// <summary>
        /// Gets a single professor with the specified Id and maps to ProfessorDto
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Single ProfessorDto</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<ProfessorDto>> GetProfessor(int id)
        {
            try
            {
                return Ok(_mapper.Map<ProfessorDto>(
                        await _professorService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Updates an existing professor. Just the values for the professor entity. 
        /// The related data can be updated in separate endpoints.
        /// Returns a failed state if the Ids dont match or the professor does not exist.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="professor"></param>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProfessorAsync(int id, ProfessorPutDto professor)
        {
            if (id != professor.Id)
                return BadRequest();

            try
            {
                await _professorService.UpdateAsync(
                        _mapper.Map<Professor>(professor)
                    );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }   

        /// <summary>
        /// Adds a new professor to the database.
        /// </summary>
        /// <param name="professorDto"></param>
        [HttpPost]
        public async Task<IActionResult> PostProfessorAsync(ProfessorPostDto professorDto)
        {
            // Mapping done separately to use the object in created at action
            Professor professor = _mapper.Map<Professor>(professorDto);
            await _professorService.AddAsync(professor);
            return CreatedAtAction("GetProfessor", new { id = professor.Id }, professor);
        }

        /// <summary>
        /// Deletes a professor by their Id.
        /// Returns a failed state if the professor doesnt exist with the specified Id.
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProfessorAsync(int id)
        {
            try
            {
                await _professorService.DeleteByIdAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Gets all the students for a Professor.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of Students in summary format</returns>
        [HttpGet("{id}/students")]
        public async Task<ActionResult<IEnumerable<StudentSummaryDto>>> GetStudentsForProfessorAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<StudentSummaryDto>>(
                            await _professorService.GetStudentsAsync(id)
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Gets all the subjects for a Professor.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of Subjects in summary format</returns>
        [HttpGet("{id}/subjects")]
        public async Task<ActionResult<IEnumerable<SubjectSummaryDto>>> GetSubjectsForProfessorAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<SubjectSummaryDto>>(
                            await _professorService.GetSubjectsAsync(id)
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Updates the students a professor supervises. 
        /// Currently only returns an error state if the professor doesnt exist, 
        /// but there are provisions to add if a student doesnt exist as well. 
        /// This is to make error handling more extendable.
        /// </summary>
        /// <param name="studentIds"></param>
        /// <param name="id"></param>
        /// <returns>List of students in summary format</returns>
        [HttpPut("{id}/students")]
        public async Task<IActionResult> UpdateStudentsForProfessorAsync(int[] studentIds, int id)
        {
            try
            {
                await _professorService.UpdateStudentsAsync(studentIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }

        /// <summary>
        /// Updates the subjects a professor teaches. 
        /// Currently only returns an error state if the professor doesnt exist, 
        /// but there are provisions to add if a subject doesnt exist as well. 
        /// This is to make error handling more extendable.
        /// </summary>
        /// <param name="subjectIds"></param>
        /// <param name="id"></param>
        /// <returns>List of subjects in summary format</returns>
        [HttpPut("{id}/subjects")]
        public async Task<IActionResult> UpdateSubjectsForProfessorAsync(int[] subjectIds, int id)
        {
            try
            {
                await _professorService.UpdateSubjectsAsync(subjectIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
    }
}
