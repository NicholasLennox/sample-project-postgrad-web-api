﻿using System;
using System.Collections.Generic;

namespace FullApiWithDocAndMapperDemo.Models
{
    public partial class Project
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public int StudentId { get; set; }

        public virtual Student Student { get; set; } = null!;
    }
}
