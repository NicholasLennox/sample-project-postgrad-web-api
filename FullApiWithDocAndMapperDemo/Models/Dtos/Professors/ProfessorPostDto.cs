﻿namespace FullApiWithDocAndMapperDemo.Models.Dtos.Professors
{
    /// <summary>
    /// DTO for the process of adding a new professor.
    /// The related data can be updated via different endpoints/funtionalities.
    /// </summary>
    public class ProfessorPostDto
    {
        public string Name { get; set; } = null!;
        public string Field { get; set; } = null!;
    }
}
