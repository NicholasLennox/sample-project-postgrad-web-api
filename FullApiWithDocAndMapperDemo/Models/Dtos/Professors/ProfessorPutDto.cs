﻿namespace FullApiWithDocAndMapperDemo.Models.Dtos.Professors
{
    /// <summary>
    /// DTO for updating an existing professor. The related data is updated via a separate endpoint.
    /// </summary>
    public class ProfessorPutDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Field { get; set; } = null!;
    }
}
