﻿namespace FullApiWithDocAndMapperDemo.Models.Dtos.Professors
{
    /// <summary>
    /// General DTO for professor, which includes all related data as Ids.
    /// </summary>
    public class ProfessorDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string Field { get; set; } = null!;
        public List<int> Students { get; set; } = null!; // list of student ids
        public List<int> Subjects { get; set; } = null!; 
    }
}
