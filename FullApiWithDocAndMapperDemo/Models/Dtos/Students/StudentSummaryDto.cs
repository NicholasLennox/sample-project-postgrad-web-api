﻿namespace FullApiWithDocAndMapperDemo.Models.Dtos.Students
{
    /// <summary>
    /// DTO for a student without any related data.
    /// Currently the only use for this is when we get Students for a professor.
    /// If we needed to get a student, or students in a StudentController, 
    /// we want to include all the related data similar to ProfessorDto. That would be a separate DTO.
    /// </summary>
    public class StudentSummaryDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
