﻿namespace FullApiWithDocAndMapperDemo.Models.Dtos.Subjects
{
    /// <summary>
    /// DTO for a subject without any related data.
    /// Currently the only use for this is when we get Subjects for a professor.
    /// If we needed to get a subject, or subjects in a SubjectsController, 
    /// we want to include all the related data similar to ProfessorDto. That would be a separate DTO.
    /// </summary>
    public class SubjectSummaryDto
    {
        public int Id { get; set; }
        public string SubCode { get; set; } = null!;
        public string SubTitle { get; set; } = null!;
    }
}
