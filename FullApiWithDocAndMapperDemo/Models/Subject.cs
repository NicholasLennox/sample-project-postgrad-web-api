﻿using System;
using System.Collections.Generic;

namespace FullApiWithDocAndMapperDemo.Models
{
    public partial class Subject
    {
        public Subject()
        {
            Students = new HashSet<Student>();
        }

        public int Id { get; set; }
        public string SubCode { get; set; } = null!;
        public string SubTitle { get; set; } = null!;
        public int? ProfessorId { get; set; }

        public virtual Professor? Professor { get; set; }

        public virtual ICollection<Student> Students { get; set; }
    }
}
