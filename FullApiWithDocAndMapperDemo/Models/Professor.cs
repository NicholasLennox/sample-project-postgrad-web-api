﻿using System;
using System.Collections.Generic;

namespace FullApiWithDocAndMapperDemo.Models
{
    public partial class Professor
    {
        public Professor()
        {
            Students = new HashSet<Student>();
            Subjects = new HashSet<Subject>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Field { get; set; }

        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Subject> Subjects { get; set; }
    }
}
