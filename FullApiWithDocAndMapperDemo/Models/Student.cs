﻿using System;
using System.Collections.Generic;

namespace FullApiWithDocAndMapperDemo.Models
{
    public partial class Student
    {
        public Student()
        {
            Subjects = new HashSet<Subject>();
        }

        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public int? ProfessorId { get; set; }

        public virtual Professor? Professor { get; set; }
        public virtual Project? Project { get; set; }

        public virtual ICollection<Subject> Subjects { get; set; }
    }
}
